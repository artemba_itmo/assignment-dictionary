%include "colon.inc"

section .rodata

colon "agency", agency_word
db "a business or organization providing a particular service on behalf of another business, person, or group.", 0

colon "company", company_word
db "a commercial business.", 0

colon "state", state_word
db "a nation or territory considered as an organized political community under one government.", 0