%include "lib.inc"

global find_word

section .text

; arguments:
;	rdi - pointer to a null terminated key string  
;	rsi - pointer to the first record in the dictionary
; find_word will go through the whole dictionary, comparing a given key with each key in dictionary. 
; If the record is not found, it returns zero, otherwise it returns record address.
; rax - return value
find_word:

	xor rax, rax
	cmp rsi, 0					
	je .end 					; list is empty

	.loop:
		add rsi, 8				; point to key field of the current record
		call string_equals		
		sub  rsi, 8				; move back to point to list_pointer field
		cmp rax, 0
		je .next_list_record
		mov rax, rsi			; pointer to current record -> rax
		jmp .end
	
		.next_list_record:
		cmp qword [rsi], 0		; check list_pointer field
		je .end					; last lits record reached
		mov rsi, [rsi]			; go to next list record
		jmp .loop
	
	.end:
	ret