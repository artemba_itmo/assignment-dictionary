%include "lib.inc"
%include "words.inc"
extern find_word


global _start


section .data
str_ask_input:		db 10,'Type a word to explain:', 10, 0
str_explanation:	db 'Explanation: ', 0
str_err_input:		db 'Input error', 10, 0
str_err_not_found :	db 'Record not found', 10, 0
new_line: 			db 10, 0


section .bss
%assign WORD_BUF_SIZE (255 + 1) ; for null-terminated input string, max 255 characters, better in section .bss
word_buf: times WORD_BUF_SIZE db


section .text

%define stdout 1
%define stderr 2

_start: 

	.loop:
		; print str_ask_input
		mov rdi, str_ask_input
		call print_string
		
		; read a word from stdin
		mov rdi, word_buf
        mov rsi, WORD_BUF_SIZE 
		call read_word; in rdx returns input string lenght
		cmp rax, 0
		je .print_error_input
		
		; try to find input word in the list 
		mov rsi, list_pointer
		call find_word
		cmp rax, 0; if the record is not found, it returns in rax zero; otherwise it returns record address.
		je .print_error_not_found

		; move pointer to value field of the found list record
		mov rsi, rax; pointer to the found record put in rsi
		add rsi, 8; move pointer to key field of the found list record
		add rsi, rdx; add key field length to access value field
		add rsi, 1; bypass null-terminator of key field
		
		; print str_explanation
		push rsi 
		mov rdi, str_explanation
		call print_string
		pop rsi
				
		; print value field of the found list record
		mov rdi, rsi
		call print_string		

		; print new line symbol
		call print_newline
		jmp .next_input
		
		.print_error_input:
		mov rdi, str_err_input
		call print_err
		jmp .next_input
	
		.print_error_not_found:
		mov rdi, str_err_not_found
		call print_err
		jmp .next_input
	
		.next_input:
		jmp .loop

	call exit
	
	
	
	print_err:
    call string_length
    mov rdx, rax; длина строки rax -> rdx (равняется кол-ву байт для вывода)
    mov rax, 1; write system call -> rax
    mov rsi, rdi; адрес первого байта последовательности, кот. нужно вывести -> rsi
    mov rdi, stderr; stderr (2)
    syscall
    ret