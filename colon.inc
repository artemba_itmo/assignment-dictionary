%define list_pointer 0; will be used as an address of the first element of the list

; list record structure:
; [list_pointer field (8-bytes)] -> [key field (variable-lenght 0-terminated str)] -> [value field (variable-lenght 0-terminated str)] 

; takes two args: 1 - key field, 2 - label for value field
%macro colon 2 

	; "local(!!!) label in macro" will be generated unique for each list record because a specific prefix is added
	%%current_list_record_lable:
	
	; link to the next record of the list, 0 if the last/single record in the list. dq because address is 8 bytes 
	dq list_pointer 

	; allocate key field
	db %1, 0 
	
	; only(!!!) label for value field. Value field itself is following immediately after macros usage
	%2:  

	; redefine list pointer to point to the current (last) list record
	%define list_pointer %%current_list_record_lable

%endmacro