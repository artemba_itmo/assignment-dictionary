main: main.o lib.o dict.o
	ld --dynamic-linker=/lib64/ld-linux-x86-64.so.2 main.o lib.o dict.o -o main

main.o: main.asm words.inc colon.inc lib.inc
	nasm -felf64 main.asm -o main.o	

lib.o: lib.asm
	nasm -felf64 lib.asm -o lib.o
	
dict.o: dict.asm lib.inc
	nasm -felf64 dict.asm -o dict.o

clean:
	rm -rf *.o main